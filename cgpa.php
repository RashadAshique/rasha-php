<!DOCTYPE html>
<html>
<head>
    <title>GPA Calculator</title>
      <link href='https://fonts.googleapis.com/css?family=Nunito' rel='stylesheet' type='text/css'>
      <link rel="icon" type="image/jpg" href="images1/auction2.jpg">
      <link rel="stylesheet" type="text/css" href="cgpa.css">
      <style type="text/css">
      .cg{
        color: #6600cc;
        font-size: 40px;
        text-align:center;
        font-family: Nunito;
     }

    </style> 
</head>
<body>
   <?php
$db = $micro = $math = $dsd = $hum = $dblab = $microlab = $dsdlab = $phplab = "";
if (isset($_POST['submit'])){
if($_SERVER["REQUEST_METHOD"] == "POST"){
$db = $_POST["db"];
$micro = $_POST["micro"];
$math = $_POST["math"];
$dsd = $_POST["dsd"];
$hum = $_POST["hum"];
$dblab = $_POST["dblab"];
$microlab = $_POST["microlab"];
$dsdlab = $_POST["dsdlab"];
$phplab = $_POST["phplab"];
}

if($db<40)
        {
            $x = 0;
        }
        else if($db>=40 && $db<45)
        {
            $x = 2;
        }
         else if($db>=45 && $db<50)
        {
            $x = 2.25;
        }
         else if($db>=50 && $db<55)
        {
            $x = 2.5;
        }
         else if($db>=55 && $db<60)
        {
            $x = 2.75;
        }
         else if($db>=60 && $db<65)
        {
            $x = 3;
        }
         else if($db>=65 && $db<70)
        {
            $x = 3.25;
        }
         else if($db>=70 && $db<75)
        {
            $x = 3.5;
        }
         else if($db>=75 && $db<80)
        {
            $x = 3.75;
        }
         else if($db>=80 && $db<=100)
        {
            $x = 4;
        }

    if($micro<40)
        {
            $y = 0;
        }
        else if($micro>=40 && $micro<45)
        {
            $y = 2;
        }
         else if($micro>=45 && $micro<50)
        {
            $y = 2.25;
        }
         else if($micro>=50 && $micro<55)
        {
            $y = 2.5;
        }
         else if($micro>=55 && $micro<60)
        {
            $y = 2.75;
        }
         else if($micro>=60 && $micro<65)
        {
            $y = 3;
        }
         else if($micro>=65 && $micro<70)
        {
            $y = 3.25;
        }
         else if($micro>=70 && $micro<75)
        {
            $y = 3.5;
        }
         else if($micro>=75 && $micro<80)
        {
            $y = 3.75;
        }
         else if($micro>=80 && $micro<=100)
        {
            $y = 4;
        }
     if($math<40)
        {
            $a = 0;
        }
        else if($math>=40 && $math<45)
        {
            $a = 2;
        }
         else if($math>=45 && $math<50)
        {
            $a = 2.25;
        }
         else if($math>=50 && $math<55)
        {
            $a = 2.5;
        }
         else if($math>=55 && $math<60)
        {
            $a = 2.75;
        }
         else if($math>=60 && $math<65)
        {
            $a = 3;
        }
         else if($math>=65 && $math<70)
        {
            $a = 3.25;
        }
         else if($math>=70 && $math<75)
        {
            $a = 3.5;
        }
         else if($math>=75 && $math<80)
        {
            $a = 3.75;
        }
         else if($math>=80 && $math<=100)
        {
            $a = 4;
        }
       if($dsd<40)
        {
            $b = 0;
        }
        else if($dsd>=40 && $dsd<45)
        {
            $b = 2;
        }
         else if($dsd>=45 && $dsd<50)
        {
            $b = 2.25;
        }
         else if($dsd>=50 && $dsd<55)
        {
            $b = 2.5;
        }
         else if($dsd>=55 && $dsd<60)
        {
            $b = 2.75;
        }
         else if($dsd>=60 && $dsd<65)
        {
            $b = 3;
        }
         else if($dsd>=65 && $dsd<70)
        {
            $b = 3.25;
        }
         else if($dsd>=70 && $dsd<75)
        {
            $b = 3.5;
        }
         else if($dsd>=75 && $dsd<80)
        {
            $b = 3.75;
        }
         else if($dsd>=80 && $dsd<=100)
        {
            $b = 4;
        }
    if($hum<40)
        {
            $c = 0;
        }
        else if($hum>=40 && $hum<45)
        {
            $c = 2;
        }
         else if($hum>=45 && $hum<50)
        {
            $c = 2.25;
        }
         else if($hum>=50 && $hum<55)
        {
            $c = 2.5;
        }
         else if($hum>=55 && $hum<60)
        {
            $c = 2.75;
        }
         else if($hum>=60 && $hum<65)
        {
            $c = 3;
        }
         else if($hum>=65 && $hum<70)
        {
            $c = 3.25;
        }
         else if($hum>=70 && $hum<75)
        {
            $c = 3.5;
        }
         else if($hum>=75 && $hum<80)
        {
            $c = 3.75;
        }
         else if($hum>=80 && $hum<=100)
        {
            $c = 4;
        }
    if($dblab<40)
        {
            $d = 0;
        }
        else if($dblab>=40 && $dblab<45)
        {
            $d = 2;
        }
         else if($dblab>=45 && $dblab<50)
        {
            $d = 2.25;
        }
         else if($dblab>=50 && $dblab<55)
        {
            $d = 2.5;
        }
         else if($dblab>=55 && $dblab<60)
        {
            $d = 2.75;
        }
         else if($dblab>=60 && $dblab<65)
        {
            $d = 3;
        }
         else if($dblab>=65 && $dblab<70)
        {
            $d = 3.25;
        }
         else if($dblab>=70 && $dblab<75)
        {
            $d = 3.5;
        }
         else if($dblab>=75 && $dblab<80)
        {
            $d = 3.75;
        }
         else if($dblab>=80 && $dblab<=100)
        {
            $d = 4;
        }
     if($microlab<40)
        {
            $e = 0;
        }
        else if($microlab>=40 && $microlab<45)
        {
            $e = 2;
        }
         else if($microlab>=45 && $microlab<50)
        {
            $e = 2.25;
        }
         else if($microlab>=50 && $microlab<55)
        {
            $e = 2.5;
        }
         else if($microlab>=55 && $microlab<60)
        {
            $e = 2.75;
        }
         else if($microlab>=60 && $microlab<65)
        {
            $e = 3;
        }
         else if($microlab>=65 && $microlab<70)
        {
            $e = 3.25;
        }
         else if($microlab>=70 && $microlab<75)
        {
            $e = 3.5;
        }
         else if($microlab>=75 && $microlab<80)
        {
            $e = 3.75;
        }
         else if($microlab>=80 && $microlab<=100)
        {
            $e = 4;
        }   
    if($dsdlab<40)
        {
            $f = 0;
        }
        else if($dsdlab>=40 && $dsdlab<45)
        {
            $f = 2;
        }
         else if($dsdlab>=45 && $dsdlab<50)
        {
            $f = 2.25;
        }
         else if($dsdlab>=50 && $dsdlab<55)
        {
            $f = 2.5;
        }
         else if($dsdlab>=55 && $dsdlab<60)
        {
            $f = 2.75;
        }
         else if($dsdlab>=60 && $dsdlab<65)
        {
            $f = 3;
        }
         else if($dsdlab>=65 && $dsdlab<70)
        {
            $f = 3.25;
        }
         else if($dsdlab>=70 && $dsdlab<75)
        {
            $f = 3.5;
        }
         else if($dsdlab>=75 && $dsdlab<80)
        {
            $f = 3.75;
        }
         else if($dsdlab>=80 && $dsdlab<=100)
        {
            $f = 4;
        }
     if($phplab<40)
        {
            $g = 0;
        }
        else if($phplab>=40 && $phplab<45)
        {
            $g = 2;
        }
         else if($phplab>=45 && $phplab<50)
        {
            $g = 2.25;
        }
         else if($phplab>=50 && $phplab<55)
        {
            $g = 2.5;
        }
         else if($phplab>=55 && $phplab<60)
        {
            $g = 2.75;
        }
         else if($phplab>=60 && $phplab<65)
        {
            $g = 3;
        }
         else if($phplab>=65 && $phplab<70)
        {
            $g = 3.25;
        }
         else if($phplab>=70 && $phplab<75)
        {
            $g = 3.5;
        }
         else if($phplab>=75 && $phplab<80)
        {
            $g = 3.75;
        }
         else if($phplab>=80 && $phplab<=100)
        {
            $g = 4;
        }   
  $result = (3*$x + 3*$y + 3*$a + 3*$b + 3*$c + 1.5*$d + .75*$e + .75*$f + .75*$g)/18.75;
  
  echo "<div class='cg'>Your obtained GPA in this semester is"." ".$result."</div>";
 } 
?>
 
<div class="reg">
	<p>GPA CALCULATOR</p>
	<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
			<legend>
				Give Your Marks
			</legend><br>
			Database &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp <input type="text" name="db" placeholder="enter your marks"> 
			<br> <br>
			Microprocessor &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<input type="text" name="micro" placeholder="enter your marks"> 
			<br> <br>
			Mathematics &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp <input type="text" name="math" placeholder="enter your marks">
			<br> <br> 
			DSD &nbsp&nbsp&nbsp &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<input type="text" name="dsd" placeholder="enter your marks"> 
			<br> <br>
			Humanities &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<input type="text" name="hum" placeholder="enter your marks"> 
			<br> <br>
			Database Lab &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<input type="text" name="dblab" placeholder="enter your marks"> 
			<br> <br>
			Microprocessor Lab &nbsp&nbsp&nbsp<input type="text" name="microlab" placeholder="enter your marks"> 
			<br> <br>
			DSD Lab &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<input type="text" name="dsdlab" placeholder="enter your marks"> 
			<br> <br>
			PHP Lab &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<input type="text" name="phplab" placeholder="enter your marks"> 
			<br> <br>
			<input  type="submit" name="submit" value="Check GPA"><br><br>
	</form>
</div>
</body>
</html>
